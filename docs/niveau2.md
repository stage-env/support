# Niveau 2 : Environnement de Travail du CREMI

Au programme :

* Notions de shell  et de système de fichiers Unix
* Éditeur de texte (nano)
* Hello world : compilation C, script Python et page web (index.html)

## Interpréteur de commandes Bash

### Introduction

Un interpréteur de commandes est un processus dont le rôle est de fournir une
interface textuelle avec le système d'exploitation. Il peut être lancer dans une
fenêtre (appelé terminal ou console) ou directement lorsqu'on travaille sans
interface graphique (de haut niveau). Un peu de vocabulaire : les interpréteurs
de commandes Unix sont appelés  des *shells*.

* Lancez un interpréteur de commande en cliquant sur l'icône symbolisant un
  écran d'ordinateur (des années soixante).
* Tapez ce qu'il vous passe dans la tête puis la touche entrée, simplement pour
  voir ce qu'il se passe.

![Image Shell](data/shell.gif)

Un interpréteur de commandes *en mode console* exécute en boucle la séquence
d'actions suivante :

* affichage de l'invite de commande (*prompt*) signalant qu'il est en attente
  d'une nouvelle commande de la part de l'utilisateur ;
* aide à la saisie de la nouvelle commande, en fournissant à l'utilisateur des
  fonctions d'édition de texte en mode ligne : accès à l'historique des
  commandes précédentes, copie et remplacement rapides, complétion de noms de
  fichiers, etc. ;
* analyse et vérification syntaxique de la commande tapée par l'utilisateur, et
  affichage d'un message d'erreur en cas de problème ;
* si la commande est conforme, un appel au système d'exploitation est effectué
  pour réaliser les opérations demandées : lancement d'un nouveau processus, etc. ;
* attente de la fin de la commande en cours.

Plus d'info : <http://fr.wikipedia.org/wiki/Bourne-Again_shell>

### Édition des Commandes

Dans une utilisation interactive du shell, c’est-à-dire lorsque le shell
interprète ligne à ligne les commandes soumises par l'utilisateur, il est
important de pouvoir corriger au besoin la ligne de commande avant de la
soumettre. Cela se fait à l'aide de commandes similaires à celles d'un éditeur
de texte (déplacement du curseur, suppression de caratères, etc).

* Tapez ```who``` puis sur la touche `Enter`, puis recommencez avec la commande
  ``whoami``.
* Tapez la commande ```cal``` suivi de votre année de naissance puis sur la
  touche `Enter`.
* Tapez ```whoa``` puis sur la touche de tabulation (```Tab```, à gauche du
  clavier avec les deux flèches en sens opposés) puis sur entrée. Cela permet
  ainsi de compléter automatiquement sans avoir besoin de tout taper. C'est très
  efficace quand on en prend l'habitude !
* Pour rechercher dans l'historique des commandes, tapez la combinaison de
  touches  ```Ctrl-r``` puis la lettre ```w``` - en tapant plusieurs fois
  ```Ctrl-r```  vous voyez défiler les commandes contenant la lettre `w`.
  * Utilisez les touches de déplacement du curseur pour naviguer dans la ligne
    de commande ainsi que dans l'historique des commandes.

  Quelle que soit la position du curseur sur la ligne de commande, la touche
  `Enter` lance l'interprétation de la ligne par le shell.  Une autre
  possibilité de bash, qui concerne encore une fois la construction d'une
  commande, permet la **complétion** du mot commencé grâce à la touche `Tab`.

  * Tapez ```cd``` puis utilisez la touche `Tab` pour voir l'action de la
    complétion.


## Système de Gestion de Fichiers Unix

### Introduction

  Le système de gestion de fichiers (SGF) d'Unix procure à l'utilisateur un
  moyen efficace pour conserver et manipuler aisément des informations. En
  outre, il offre un système de sécurité, notamment sur les droits d'accès aux
  fichiers. Il existe trois principaux types de fichiers :

* les fichiers de données (ordinary files),
* les répertoires (directories),
* les périphériques (devices).

Ce SGF est simple et permet de manipuler de manière uniforme les fichiers comme
les périphériques. Sous Unix, on a coutume de dire que *tout est fichier !* Par
ailleurs, le SGF d'Unix ne fait aucune supposition sur l'organisation interne
des fichiers. Tout fichier est vu comme une simple suite d'octets.

Le système de gestion de fichiers utilise une structure hiérarchique
(arborescence) composée de répertoires et de fichiers. Chaque
répertoire contient des fichiers ordinaires ou d'autres répertoires.

### Les répertoires

Il existe un certains nombre de répertoires particuliers :

```
/    racine de l'arborescence,
.    répertoire courant,
..   répertoire père du répertoire courant,
~    votre répertoire utilisateur (home directory)
```

La variable ```$HOME``` pointe également sur votre répertoire utilisateur,
également appelé répertoire de connexion. Il s'agit en quelque sorte de votre
maison, là où vous pouvez stocker vos fichiers. Au CREMI, sauf changement de
votre part, il est en lecture et écriture uniquement pour vous.

* Pour afficher le contenu de la variable HOME, dans un terminal
tapez la commande : ```echo $HOME```.

Nota Bene : Le symbole ```$``` placé devant le nom de la variable signifie que
vous souhaitez accèder à la valeur de la variable.

### Nom de Fichier

Les noms de fichiers sont limités à 256 caractères sous Unix. De préférence,
n'utilisez pas d'espace mais `"_"` ou `"-"` à la place. Evitez les caractères 
spéciaux (&, @, $, #, ...). Le plus simple est de toujours utiliser des lettres et des
chiffres.

Attention le système Unix fait la différence entre majuscules et minuscules !
Les fichiers `toto`, `Toto` et `TOTO` ont des noms différents, contrairement à Windows.

L'extension ou suffixe (optionnel) fait partie du nom, il commence par ```.```
et n'a pas de limite de taille (```.txt```, ```.html```, ```.tar.gz```,
```.ps.gz```, etc.). Il permet d'indiquer le type du fichier. Il ne s'agit que
d'une convention sous Unix. Aussi changer l'extension d'un fichier ne modifie
pas son contenu : c'est  simplement un changement de nom qui peut, cependant,
modifier le comportement des programmes tenant compte des extensions.

Pour connaître le type d'un fichier, il faut utiliser la commande ```file```.
Par exemple :

```bash
file ~/.bashrc
```

### Chemins Relatif et Absolu

Un chemin (*path*) est la succession de répertoires qu’il faut traverser
jusqu’à l’objet que l’on veut atteindre. Ce chemin peut être absolu s’il part
de la racine (notée ```/```) du système de fichier, ou encore relatif s’il part
du répertoire d’où l’on effectue la commande (répertoire courant du processus
exécutant la commande).

```
   /
   ├──net/
   │   ├── cremi/
   │   │   ├── A/
   │   │   ├── B/
   │   │   │   └── quux
   │   │   └── C/
```

Ainsi ```/net/cremi/B/quux``` est un chemin absolu qui désigne le fichier quux.
Le chemin relatif ```../B/quux``` désigne le fichier quux  lorsque le répertoire
courant est soit ```/net/cremi/A```, soit ```/net/cremi/B```  ou encore
```/net/cremi/C```.  Notons également que le chemin ```quux```désigne le fichier
quux lorsque le répertoire courant est ```/net/cremi/B```.

### Quelques mots sur le SGF du CREMI

Le SGF du CREMI est réparti sur les disques des machines de travail et
différents ensembles de disques de données (baies de stockage) pour des raisons
de performance, de coût, de sécurité et de fiabilité. Votre répertoire
d'utilisateur est stocké sur la baie la plus rapide, cependant vous n'avez droit
d'y conserver que 2000 Mo de données. Pour stocker jusqu'à 75Go, vous pouvez
utiliser le répertoire ~/espaces/travail qui désigne un répertoire stocké sur
une baie peu plus lente mais de capacité supérieure.

```
├── ~
│   ├── espaces/
│   │   ├── doc/
│   │   ├── ens/
│   │   ├── travail/  # stocker ici tous vos projets
│   │   └── www/
```

Pour vos fichiers temporaires vous avez toujours intérêt à utiliser le
répertoire ```/tmp``` qui est automatiquement nettoyé à chaque démarage de la
machine.

Le SGF du CREMI est présenté dans cet article
<https://services.emi.u-bordeaux.fr/intranet/spip.php?article6>  où l'on vous
indique, par exemple, où stocker vos pages html pour les publier sur le web du
CREMI.

**À noter** : Vos données stockées dans votre répertoire d'accueil tout comme
celles de votre espace de travail sont sauvegardées plusieurs fois par jour
(approximativement à heure fixe) et plusieurs sauvegardes sont conservées. On
peut ainsi remonter dans le temps pour accèder à quelques anciennes versions :
une vingtaine de *point de sauvegardes* espacés dans le temps sont accessibles.
Les sauvegardes d'un répertoire sont accessibles sous le répertoire `./.ckpt/`
(pour checkpoint = point de sauvegarde).

```bash
$ ls .ckpt
2020_07_26_03.00.04_GMT
2020_08_30_23.00.06_GMT
2020_09_05_01.00.03_GMT
2020_08_02_03.00.04_GMT
2020_08_31_23.00.04_GMT
2020_09_05_02.00.03_GMT
2020_08_09_03.00.03_GMT
2020_09_01_23.00.04_GMT
2020_09_05_03.00.05_GMT
2020_08_16_03.00.05_GMT
2020_09_02_23.00.02_GMT
2020_09_05_04.00.03_GMT
2020_08_23_03.00.03_GMT
2020_09_03_23.00.03_GMT
2020_09_05_05.00.03_GMT
2020_08_29_23.00.03_GMT
2020_09_04_23.00.03_GMT
2020_09_05_06.00.04_GMT
2020_08_30_03.00.05_GMT
2020_09_05_00.00.03_GMT
```

Le nom du répertoire de sauvegarde correspond à la date de la sauvegarde, à la seconde près.

## Commandes Unix et Aide en Ligne

### Format et manuel d'une commande

Voici la syntaxe classique d'une commande Unix (ici appelée cmd, à remplacer par les nom de commandes décrites plus bas) :

```
cmd [-opt1] [-opt2] ... [--] arg1 arg2 ...
```

où :

* ```cmd``` correspond au nom de la commande,
* ```-opt``` correspond à une option possible,
* ```arg``` correspond à un argument.

Notons que les arguments sont le plus souvent un nom de fichier que la
commande manipule. On peut utiliser ```--``` pour séparer explicitement
les options des arguments quand la commande est ambiguë.

Pour obtenir l'aide en ligne sur une commande, il suffit d'utiliser le
```man```. Pour obtenir l'aide sur une commande cmd, il suffit de taper :

```
man cmd
```

(en remplaçant bien sûr cmd par le nom de la commande voulue)

Attention ! L'aide est écrite en anglais et les version traduites sont
souvent de moindre qualité. Un conseil : s'habituer dès maintenant à
lire la documentation en anglais ;-)

* Demander l'aide de la commande mkdir.
* Demander l'aide de la commande man.

Faire défiler avec les flèches haut/bas, utiliser "q" pour quitter. Pour
chercher plus rapidement ce qui vous intéresse, taper "/" puis le texte à
chercher, puis entrée, ce qui vous amène à la première occurrence. Pour
passer aux occurrences suivantes, taper "n" (comme next).

### Gestion de l'arborescence

```
cd     changer de répertoire courant (change directory)
mkdir  Crée un répertoire (make directory)
rmdir  Supprime un répertoire (remove directory)
ls     Liste le contenu d'un répertoire (list)
pwd    Affiche le chemin du répertoire courant (path working directory)
find   Recherche des fichiers ou des répertoires d'après leurs propriétés
```

* Dans quel répertoire se trouve l'interpréteur de commande ?
* Lister les fichiers contenus dans ce répertoire.
* Dans un terminal, utilisez mkdir pour créer le répertoire TOTO. Il suffit de
  taper ```mkdir TOTO``` ; dans ce cas on dit que TOTO est un argument (ou
  paramètre) de la commande mkdir.
* Grâce à la commande ```ls -l```, vérifiez que vous avez bien créé ce
  répertoire (vérifiez la date).
* Détruisez ce répertoire en utilisant la commande ```rmdir```.
* Tapez la commande ```cd```. Cette commande, sans paramètre, a pour effet de
  vous re-positionner dans votre répertoire utilisateur.
* Qu'y a-t-il dans le répertoire ```$HOME/..``` ? Avez-vous trouvé le répertoire
  de votre voisin ?
* Essayez de vous déplacer dans le répertoire de votre voisin, et de lister ses
  fichiers.
* Essayez de lister le répertoire ```~/.ckpt``` contenant les sauvegardes de
  votre répertoire d'acceuil.
* Essayez de vous placer dans le répertoire ```~/.ckpt``` puis  de lister son
     contenu.

### Lister les Fichiers

Vous allez maintenant voir un peu plus en détail comment lister les
fichiers contenus dans un répertoire. La commande utile pour cet
exercice est ls.

Dans le répertoire ```/bin``` (ou ```/usr/bin```) :

* Listez tous les fichiers y compris les fichiers cachés
(commençant par un point) ;
* Listez tous les fichiers en format long ;
* Listez les fichiers du plus ancien au plus récent en format long ;

Les caractères suivants ont une signification particulière pour l'interpréteur
de commandes (i.e. le shell) : ? * [ ] \ ˜. Même si on le déconseille, ces
caractères peuvent néammoins être utilisés dans des noms de fichiers ou
répertoires en les _despécialisant_ à l'aide du caractère \\.

* le caractère ```?``` permet de remplacer un caractère quelconque ; parexemple,
  la commande ```ls fic?```  donnera tous les noms de quatre lettres dont les
  trois premières sont fic ;
* le caractère ```*``` remplace n'importe quelle chaîne de caractères (y compris
  la chaîne vide) ; par exemple, la commande ```ls fic*``` donnera tous les noms
  de trois lettres ou plus, dont les trois premières lettres sont fic ;
* une suite de caractères entre crochets ```[ ]``` désigne un seul caractère de
  la suite ; par exemple, en supposant que vous disposiez dans votre répertoire
  courant des fichiers fic1, fic2 et fic3, alors ```ls fic[123]``` sera
  équivalent à ```ls fic1 fic2 fic3``` ;*
* deux caractères séparés par un - entre crochets [ ] (par exemple [a-e])
  désigne un seul caractère de l'intervalle de caractères ; par exemple, en
  supposant que vous disposiez dans votre répertoire courant des fichiers fica,
  ficb, ficc, ficd, et fice, alors la commande ```ls fic[a-e]``` est équivalente
  à ```ls fica ficb ficc ficd fice```.

Rappel : le caractère ```˜``` désigne le chemin du répertoire d'accueil de
  l'utilisateur courant.

* Listez tous les fichiers dont le nom :
  1. commence par r ;
  2. finit par e ;
  3. commence par y et finit par l
  4. contient un chiffre entre 2 et 9.
* Avec la commande ```find```, listez tous les fichiers qui commencent par un
  point et se termine par rc sur votre compte utilisateur.
* Listez maintenant tous les fichiers qui font plus de 1Mo sur votre compte utilisateur.

### Manipulation de Fichiers


```
touch Crée un fichier vide s’il n’existe pas, et modifie le atime sinon
cp    Copie des fichiers ou des répertoires (copy)
mv    Déplace ou renomme un fichier ou un répertoire (move)
rm    Supprime un fichier (remove)
cat   Affiche le contenu de fichiers
less  Affiche le contenu de fichiers de façon interactive
```

* Reproduire la hiérarchie suivante :

```
├── /tmp
│   ├── foo/
│   │   ├── bar/
│   │   ├── quux/
│   │   │   └── baz
│   │   ├── rny/
│   │   │   ├── heak
│   │   │   └── gpq

```

* Avec la commande ```cp```, faites une copie récursive du répertoire ```foo/rny/ vers foo/ohka/```.
* Avec la commande ```mv```, renommez et déplacez le fichier ```foo/rny/heak``` en ```foo/ohka/zab```.
* Avec la commande ```rm```, effacez le répertoire quux/ et tout ce qu’il contient récursivement.
* Avec la commande ```find```, trouvez tous les fichiers dans ```foo/``` qui ont un ’a’ dans leur nom.
* Utilisez les commandes ```cat``` puis ```less``` pour visualiser le contenu du fichier ```/etc/vnc.conf``` .
* Supprimez le répertoire ```foo```.

### Recherche de chaînes de caractères

```
grep  Recherche de motifs dans un ensemble de fichiers
```

* Listez toutes les lignes contenant le mot HOME dans les fichiers d'extension ```.conf``` du répertoire ```/etc```.

---

## Éditeurs de textes

### Nano : un éditeur en mode console

Tout informaticien doit savoir utiliser un éditeur en mode console -
c'est-à-dire sans interface graphique évoluée - car ce peut être le _seul_
éditeur disponible en mode dégradé  (machines distantes, machines à moitié
cassées) et parce que ce peut être tout simplement _le meilleur outil_ pour
éditer un fichier à un moment donné. Par exemple, on ne va pas lancer un éditeur
complexe pour modifier une ligne - ce n'est ni productif, ni écologique !

Voici les principales commandes :

```
Ctrl+G    (F1)      Afficher le message d'aide
Ctrl+X    (F2)      Fermer l'espace en cours / Quitter nano
Ctrl+O    (F3)      Écrire le fichier en cours (ou la zone marquée) sur disque
Ctrl+R    (Ins)     Insérer un autre fichier dans l'espace courant (ou un nouvel espace)
```

* Lancez dans un terminal la commande ```nano toto.txt``` - tapez un texte,
  afficher l'aide pour pouvoir le sauvegarder puis quitter nano.
* Lancez à nouveau la commande ```nano toto.txt```, modifiez le texte et sortez
  sans sauvegarder.
## Hello World

### Programme C

* En utilisant Nano, créez un fichier source C ```hello.c``` avec le code suivant : 
```c
#include <stdio.h>
int main(void) {
  printf("hello world!\n");
  return 0;
}
```
* Compilez le programme *hello.c* en entrant la commande  ```gcc hello.c``` qui
  devrait produire le fichier exécutable ```a.out```
* Lancez le programme en entrant ```./a.out```
* Tapez la  ```gcc hello.c -o hello``` et vérifier que le fichier ```hello```
  est exécutable en entrant la commande ```./hello```.

### Programme Python

* Créez de même le fichier source Python 3 ```hello.py``` contenant :
```python
print("hello world!")
```
* Lancez son interprétation en entrant ```python3 hello.py```
* Modifiez le programme ```hello.py``` en plaçant en première ligne la ligne suivante :
```
#!/usr/bin/env python3
print("hello world!")
```
* puis ajouter le droit d'exécution au fichier ```hello.py``` :
```
chmod +x hello.py
```
* Exécutez ensuite le programme en tapant ```./hello.py``` .
* Ouvrez ce fichier dans VS Code et exécutez le grâce à l'extension *Runner*...

### Page Web (code HTML)

* Créez et éditez un fichier ```~/espaces/www/index.html``` pour y placer le
  texte html correspondant au "hello world".
* Vérifiez ensuite que votre fichier est bien disponible sur le web à l'adresse
  ```https://prenom-nom.emi.u-bordeaux.fr```. Vous pouvez consulter l'intranet
  du CREMI pour connaître précisément l'adresse de votre site web :
  <https://services.emi.u-bordeaux.fr/intranet/mesinfos/>

---

## Pour aller plus loin

### Droit d'Accès aux Fichiers

A chaque fichier est associé un ensemble de permissions qui détermine qui a le
droit de lire, écrire, effacer ou exécuter un fichier. Ces droits d'accès sont
résumés par des lettres :

 - ```r``` : autorisé en lecture.
 - ```w``` : autorisé en écriture-effacement.
 - ```x``` : autorisé en exécution.

Ces trois permissions peuvent être appliquées au propriétaire du fichier, aux
membres du groupe, et à tous les autres utilisateurs du système. En résumé :

- ```u``` : user (le propriétaire du fichier).
- ```g``` : group (le groupe auquel est rattaché le fichier).
- ```o``` : others (tous les autres utilisateurs).
- ```a``` : all (le proriétaire, le group, et les autres)

Exemple : ```-rwxrwxrwx 1 toto miage 124 2010-09-08 17:59 mon_fichier```

signifie que l'utilisateur toto possède un fichier de 124 octets qui
s'appelle ```mon_fichier``` créé le 8 septembre de l'année 2010 à 17h59 et
qui appartient au groupe miage. mon_fichier est autorisé en lecture,
écriture et exécution pour le propriétaire (u=```rwx```), le groupe (g=```rwx```)
et les autres utilisateurs (o=```rwx```).

La commande Unix vous permettant de changer les permissions d'accès
aux fichiers est : ```chmod```. Pour changer les permissions de l'exemple
précédent afin d'obtenir les permissions de l'exemple suivant,
l'utilisateur toto utilisera la commande ```chmod g-w,o-wx mon
fichier```.

EXEMPLE : ```-rwxr-xr--- 1 toto miage 124 2010-09-08 17:59 mon_fichier```

signifie que l'utilisateur toto possède un fichier de 124 octets qui
s'appelle mon_fichier crée le 8 septembre de l'année 2010 à 17h59 et
qui appartient au groupe miage. mon_fichier est autorisé en lecture,
écriture et exécution pour le propriétaire (u=```rwx```), en lecture et
exécution seulement pour le groupe (g=```r-x```) et en lecture seule pour
les autres utilisateurs (o=```r---```).

Pour retrouver les permissions du premier exemple, le propriétaire du
fichier utilise la commande ```chmod g+w,o+wx mon_fichier```. Il pourrait
aussi utiliser la commande : ```chmod u=rwx,g=rwx,o=rwx mon_fichier```

Vous pouvez aussi positionner des permissions (pour vous, le groupe et
les autres) sur vos répertoires.

Dans ce cas, les lettres ```r```,```w``` et ```x``` ont la signification suivante.

- ```r``` : autorisé à lister le contenu du répertoire.
- ```w``` : autorisé à créer ou détruire des fichiers dans le répertoire.
- ```x``` : autorisé à traverser ce répertoire.

* Créez un répertoire ```~/droits```.
* Copiez le fichier ```/etc/hosts``` dans les fichiers ```~/droits/hosts``` et
  ```~/droits/hosts.bis```.
* Modifiez les protections de votre fichier ```hosts.bis``` pour que vous,
propriétaire du fichier, puissiez le lire mais pas l'effacer. Vérifiez.
* Modifiez les protections de votre fichier ```hosts.bis``` et du répertoire ```droits``` pour que les
utilisateurs de votre groupe puissent le lire et le détruire. Vérifiez.

### Les Liens Symboliques

* Déplacez-vous dans votre répertoire d'accueil. Créez un fichier
source en tapant dans un terminal les commandes suivantes :

```bash
cat > source
Hello World!
<Ctrl>-d
```

Ce fichier est créé a l'aide de la commande cat et d'un mecanisme de
redirection (>). Nous verrons dans un TP ultérieur plus en details les
mecanismes de redirection.

* Lisez la page de manuel de la commande ```ln```.
* Visualisez le contenu du fichier source.
* Créez un lien symbolique (```ln -s```) de source vers ```source.sym```.
* Listez à l'aide de la commande ```ls -l``` le contenu du répertoire ```˜```.
* Vérifiez avec la commande ```cat``` que ces deux fichiers ont le même
contenu.
* Renommez le fichier source en ```source.bis```. Qu'en est-il du lien
symbolique ?

### Archivage

Les utilitaires ```tar``` et ```gzip``` sont très utilisés pour archiver et
comprimer des fichiers.

Ainsi, la commande :
```
tar cvf ~/tmp.tar /tmp
```
permet de réunir tous les fichiers et les sous-répertoires contenus
dans ```/tmp``` dans le fichier ```~/arch.tar```. Ce fichier peut alors être comprimé pour un envoi d'email :
```
gzip ~/tmp.tar
```
On obtient alors le fichier comprimé ```~/tmp.tar.gz``` qui remplace le
fichier original.

Inversement, la commande :
```
gunzip ~/tmp.tar.gz
```
permet de décomprésser le fichier et la commande :
```
tar xvf ~/arch.tar
```

L'option z de la commande *tar* permet de (dé)compresser tout en
(dé)archivant.
```
tar cvzf ~/tmp.tar.gz /tmp
tar xvzf ~/tmp.tar.gz
````
* Créez un répertoire archive dans lequel vous copierez
(```~/.bashrc```, ```/etc/passwd```, ```/etc/bash*```, ...). Archivez puis désarchivez
votre répertoire.

---
