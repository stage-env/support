# Application graphique avec WSL

Pour lancer une application graphique dans un sous-système Linux (WSL) pour
Windows 10, il existent sur le web de nombreux tutoriels à votre disposition.
Regardez par exemple [ce
tutoriel](https://korben.info/linux-wsl-gui-interface-graphique-windows-10.html).
D'autres approches sont également possibles en utilisant une machine virtuelle
Linux dans votre Windows (*virtualbox*) ou encore en utilisant *MinGW*.

## Préambule

Considérons la solution basée sur WSL. Dans ce cas, il faut commencer par
installer un sous-système Linux ou *WSL* (par exemple *Ubuntu*) dans votre
Windows. Ce sous-système ne permet pas d'afficher nativement des programmes
graphiques, comme notre démo. Pour y arriver, il faut commencer par installer
dans votre Windows un serveur X comme
[VcXsrv](https://sourceforge.net/projects/vcxsrv/) en laissant les options par
défaut. Puis, il faut ensuite installer dans votre sous-système Ubuntu les
paquets utiles pour télécharger, compiler et exécuter la démo (git, gcc, cmake,
sdl2, ...). Attention, il ne faut pas oublier de positionner à chaque session la
variable d'environnement `DISPLAY=:0` pour indiquer que le serveur X se trouve
sur l'écran 0 de la machine Windows locale.

En résumé :

```bash
$ sudo apt install mesa-utils
$ export DISPLAY=:0
$ glxgears
```

![image glxgears](data/glxgears.png)

## En cas de problème

Si le programme *glxgears* ne s'affiche pas correctement, c'est probablement que
vous utilisez la version 2 de WSL. Pour le vérifier, il suffit de taper la
commande `wsl -l -v` dans *PowerShell*. Dans ce cas, il faut commencer par
arrêter *VcXsrv* avec les Gestionnaire de Tâches Windows, avant de le relancer
en désactivant l'option *Native OpenGL* et en activant l'option *Disable Access
Control*. De plus, dans WSL2, le sous-système Linux et la machine Windows n'ont
pas la même adresse IP ! Il faut donc corriger la variable DISPLAY de la façon
suivante : `DISPLAY=<gateway>:0` en remplaçant `<gateway>` par l'adresse IP de
la passerelle obtenue en tapant la commande `route -n`. Par exemple :

```bash
$ sudo apt install net-tools
$ route -n
Kernel IP routing table
Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
0.0.0.0         172.26.96.1     0.0.0.0         UG    0      0        0 eth0
172.26.96.0     0.0.0.0         255.255.240.0   U     0      0        0 eth0
$ export DISPLAY=172.26.96.1:0
$ glxgears
```

Plus d'info : <https://stackoverflow.com/questions/61110603/how-to-set-up-working-x11-forwarding-on-wsl2>

Si ça ne marche toujours pas, cela peut venir du *firewall* qui bloque l'accès
réseau au serveur X. Corrigez le firewall manuellement pour autoriser *VcXsrv*.
Ou plus simplement : réinstallez *VcXsrv* en veillant à valider l'exception dans
le firewall, qui est généralement proposée automatiquement lors du premier
lancement.

Une autre solution, moins élégante et moins performante, consiste à re-basculer
sur la version 1 de WSL. Il faut alors taper la commande suivante dans *PowerShell* :

```
> wsl -l -v
  NAME                   STATE           VERSION
  Ubuntu-20.04           Running         2
> wsl --set-version Ubuntu-20.04 1
```

---
