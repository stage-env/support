# Niveau 3 : Travail à Distance

**Avertissement** : Pour que ce TD soit pertinent, vous devez le réaliser sur
votre ordinateur personnel à la maison et/ou sur un ordinateur portable au
CREMI, déjà connecté au Wifi (cf. [Niveau 1](niveau1.md)).

Il nous semble important que vous puissiez apprendre à travailler correctement à
distance (depuis chez vous). Et notamment, il faut savoir :

* échanger des fichiers entre votre ordinateur personnel et votre compte au CREMI ;
* vous connecter aux machines du CREMI pour exécuter des commandes en ligne,
  éditer des fichiers, développer des programmes (C, Python, ...), les compiler
  et les exécuter, ...

Il existe de nombreuses solutions pour réaliser ces objectifs, dépendant aussi
de votre système d'exploitation : Linux, Windows, ... Ce TD vous donne quelques
pistes, mais il vous faudra probablement trouver la solution la mieux appropriée
à votre configuration particulière.

## Accès à vos fichiers du CREMI (ENT)

Voici un premier exercice facile qui nécessite juste l’utilisation d'un
navigateur Web... Commencez par vous connecter à
l'[ENT](https://ent.u-bordeaux.fr/). Puis sélectionnez le menu `Bureau > Canal
de Stockage`... Patientez quelques secondes et vous devriez avoir accès au
fichiers de votre compte informatique du CREMI.

![image canal stockage](data/ent-canal-stockage-cremi.png)

Vous pouvez naviguer dans l'arborescence de vos fichiers, télécharger un fichier
(bouton `Télécharger`) ou un répertoire entier sous forme d'une archive (bouton
`Zip`) et vous pouvez également créer de nouveau répertoire (bouton `Nv.
Dossier`), déplacer des fichiers (bouton `Copier/Couper/Coller`), ou encore
déposer un ou plusieurs fichiers de votre machine vers le CREMI (bouton
`Déposer`), etc.

**Exercice** : Faites un essai de téléchargement d'un fichier (puis d'un
répertoire) du CREMI vers votre machine et d'envoi d'un fichier de votre machine
vers le CREMI.

## Connexion SSH au CREMI

Retrouvez de nombreuses informations sur l'Intranet du CREMI :
<https://services.emi.u-bordeaux.fr/intranet/spip.php?article175>
### Prérequis

Afin de se connecter au CREMI depuis son ordinateur personnel, il est nécessaire
de vérifier quelques prérequis :

* Il faut disposer d'une connexion à Internet à la maison, ou d'un accès wifi
  sur un portable comme par exemple le Wifi Eduroam.
* Il faut disposer d'un *terminal* (sous Linux, Windows ou MacOS) qui permet
  d'interpréter des commandes en ligne (CLI).
* Il faut vérifier que l'on peut exécuter dans ce terminal la commande *ssh*,
  qui joue le rôle de client SSH.

Pour les utilisateur de Windows, il y a plusieurs manières de disposer d'un
terminal et d'un client SSH pour se connecter au CREMI :

* utiliser le client SSH natif via *l'Invite de Commande* ou de préférence le
  *PowerShell* (en principe, le composant *OpenSSH* est déjà installé) ;
* utiliser un autre client SSH (à installer), comme par exemple
  [Putty](https://www.putty.org/) ou [MobaXterm](https://mobaxterm.mobatek.net/)
  ;
* installer *WSL*, un sous-système Linux dans Windows 10 (cf. Annexes
  ci-dessous) et ensuite utiliser la commande *ssh* dans un vrai terminal Linux.

![image windows terminals](data/windows-terminals-small.png)

### Let's Rock

Si vous êtes impatients, tapez juste les commandes suivantes... Nous supposons
que vous êtes connectés sur votre ordinateur en tant que `moi@monpc` et que vous
voulez vous connecter au CREMI sur la machine passerelle
`sshproxy.emi.u-bordeaux.fr` avec votre identifiant `mylogin` :

```
# 1) test connexion SSH avec votre password UBx
moi@monpc$ ssh mylogin@sshproxy.emi.u-bordeaux.fr
mylogin@jaguar$                         # password demandé

# normalement vous êtes sur la passerelle du CREMI (jaguar, puma ou leopard)

# 2) savegardez votre ancienne configuration SSH, qui se trouve dans le répertoire .ssh/
mylogin@jaguar$ mv .ssh/ .ssh.bak/

# Notez qu'il n'est pas nécessaire d'effectuer cette commande si vous n'avez pas de répertoire ~/.ssh/ au CREMI. Sinon, elle va tout simplement échouer.

# 3) faire ensuite exit pour se déconnecter
mylogin@jaguar$ exit
moi@monpc$

# 4) génération des clés SSH
moi@monpc$ ssh-keygen
# tapez enter pour laisser les paramètres par défaut (passphrase vide)

# 5) on vérifie que les clés générés sont bien dans le répertoire .ssh/ de votre home
moi@monpc$ ls .ssh/
id_rsa                # la clé privé
id_rsa.pub            # la clé publique

# 6) on copie sa clé sur la machine distante
moi@monpc$ cp .ssh/id_rsa.pub .ssh/authorized_keys
moi@monpc$ scp -r .ssh mylogin@sshproxy.emi.u-bordeaux.fr:    # ne pas oublier le ':'

# 7) test de votre connexion SSH sans password !
moi@monpc$ ssh mylogin@sshproxy.emi.u-bordeaux.fr
mylogin@jaguar$                  # pas de password demandé !

# 8) ajouter un fichier .ssh/config sur monpc pour utiliser des alias plus efficacement
$ ssh cremi     # se connecter à la passerelle du CREMI
$ ssh infini1   # se connecter directement au serveur
```

Si vous voulez mieux comprendre et approfondir ce que font ces commandes, il
convient de  lire attentivement les 8 premières sections du [Guide
SSH](https://gforgeron.gitlab.io/ssh/guide.pdf). Cet autre
[tutoriel](https://aurelien-esnard.emi.u-bordeaux.fr/ssh/tuto-ssh.pdf) (un peu
plus court) peut également vous aider, et il est accompagné d'une petite
[vidéo](https://www.youtube.com/watch?v=tjlhLdOqx38) explicative.

**Exercice** : Depuis votre machine personnelle, connectez-vous à l'aide d'un
  client SSH à un serveur de votre choix au CREMI (jolicoeur, cocatris, infini1,
  ...) sans mot de passe (éventuellement avec une *passphrase*). Vous devez
  préalablement générer vos clés SSH, les installer au CREMI, et ajouter un
  fichier de configuration [.ssh/config](data/config).

![demo ssh](data/mini-demo-ssh.gif)

**Astuces** : Notez que les serveurs sont toujours allumés mais il n'y en a
qu'une dizaine. Si vous souhaitez disposer d'une machine rien que pour vous, il
vaut mieux dans ce cas démarrer à distance un PC du CREMI
(<https://services.emi.u-bordeaux.fr/exam/?page=wol>) avant de s'y connecter.

**Avertissement** : Attention au *fail2ban*, en cas plusieurs erreurs répétées
au moment de saisir son *password*, le CREMI va bannir votre IP pendant une
certaine durée !!!

## Copie de Fichiers avec scp

Une fois que vous avez réussi à utiliser la commande SSH pour vous connecter au
CREMI, vous pouvez apprendre à utiliser la commande *scp* pour copier des
fichiers vers/depuis une machine distante.

Voici la syntaxe générale de la commande `scp` :

```
scp [[user@]host1:]file1 ... [[user@]host2:]file2
```

Cette commande permet de copier un fichier d'une machine *host1* vers la machine
*host2*... Ccnsultez la manuel pour plus de détails : `man scp`. En général, on
l'utilise plus simplement pour copier un fichier *file1* de la machine locale
vers le home (~) distant de *user* sur la machine cible *host2* :

```
scp file1 user@host2:~/
```

Ou inversement, pour copier un fichier *file1* de la machine distante *host1*
vers le répertoire courant (.) de votre machine locale : locale :

```
scp user@host1:~/file1 .
```

**Nota Bene** : Dans chaque commande, il faut remplacer *user* par le login
associé à votre compte sur la machine source / cible.

**Exercice** : Utilisez la commande *scp* pour copier un fichier de votre
machine vers le répertoire */tmp* de la machine de votre voisin, et
inversement... Et réciproquement... Même question pour copier un répertoire en
utilisant l'option -r de la commande *scp*.

## Environnement de Développement

Bien maîtriser un ou plusieurs environnements de développement (ou IDE) est
essentiel pour réussir votre cursus en Informatique. Nous vous proposons en
Licence Informatique d'utiliser [VS Code](https://code.visualstudio.com), qui
est disponible sur la plupart des systèmes. Ils disposent également d'extension
pour travailler à distance. En outre, cet outil est déjà installé au CREMI.

### Présentation de VS Code

[VS Code](https://code.visualstudio.com) est un environnement de développement
(ou IDE), très puissant, que nous vous proposons d'utiliser au CREMI ou à la
maison (disponible sous Linux, MacOS, Windows).

Vous trouverez sur ce dépôt GIT <https://github.com/orel33/vscode> des
informations utiles sur cet outil.

VS Code utilise de nombreuses extensions. Commencez par installer les extensions
que nous vous recommandons en lançant le script
```/net/ens/vscode/extensions.sh``` au CREMI.

* Créez un répertoire ```test``` et placez-vous dedans...
* Lancez la commande ```code .``` qui va lancez VS Code en considérant le
  répertoire courant ```.``` comme l'espace de travail ou *workspace*...
* Ne jamais lancer cette commande n'importe où, et en particulier sur le
  répertoire de connexion... Car VS Code a une mauvaise habitude qui consiste à
  indexer récursivement tout les fichiers du *workspace*...
* VS Code a une tendance à utiliser beaucoup (trop) d'espace sur votre compte !
  Pour faire du ménage, lancez la commande ```/net/ens/vscode/clean.sh```. Plus
  d'info sur l'Intranet du CREMI dans cet
  [article](https://services.emi.u-bordeaux.fr/intranet/spip.php?article189).
* Afin de bénificier de plus d'espace pour votre environnement VS Code, lancez le script ```/net/ens/vscode/migrate.sh``` qui déplacera vos fichiers de configurations dans le répertoire ```~/travai/``` qui est moins limité.

Dans VS Code, débrouillez-vous maintenant pour créer un nouveau fichier
```hello.c``` avec un peu de code C....

```C
#include <stdio.h>
int main(void) {
    printf("hello world\n");
    return 0;
}
```

* Notez la complétion automatique, la coloration syntaxique, l'indentation
  automatique (Ctrl+Shift+I), ...
* Exécutez le programme, en cliquant sur le symoble ▶ (extension Runner) en haut
  à droite, qui va compiler automatiquement le programme *hello.c* et
  l'exécuter... Facile ;-)

### Travailler dans VS Code avec l'extension *Remote SSH*

Si vous disposez d'un VS Code installé sur votre machine personnel sous Windows,
Linux ou MacOS, il vous est possible grâce à l'extension *Remote SSH* d'accéder
à vos fichiers du CREMI.

**Exercice** : Reprenez le guide et lisez la section 9 du [Guide
SSH](https://gforgeron.gitlab.io/ssh/guide.pdf). Testez.

Attention, il n'est pas possible d'utiliser la passerelle du CREMI comme machine
cible. Le mieux est d'utiliser un serveur du CREMI comme *cocatris*,
préalablement déclaré dans son fichier  `.ssh/config` avec un rebond
(*proxyjump*).

### Travailler dans VS Code avec l'extension *Remote WSL*

Dans ce cas, il ne s'agit pas de travailler dans VS Code en se connectant à une
machine du CREMI (via SSH). Au contraire, il s'agit ici de travailler en local
sur son PC dans le VS Code de sa machine *Windows 10*, mais en s'appuyant sur un
sous-système Linux (WSL) pour éditer, compiler, exécuter ses programmes dans ce
sous-système.

Pour ce faire, il faut réaliser les étapes suivantes sur son PC Windows 10 :

* installer VS Code dans Windows,
* installer WSL avec un sous-système Ubuntu ou Debian (cf. Annexes ci-dessous),
* installer l'extension [Remote WSL](https://code.visualstudio.com/docs/cpp/config-wsl) dans VS Code,

Une fois installé, lancez VS Code et connectez-vous à votre sous-système Linux
(Ubuntu) en l'extension *Remote WSL*. En pratique, il suffit de cliquer sur le
bouton vert `><` en bas à gauche de la barre d'état. Une autre solution consiste
à lancer directement la commande `code .` depuis le terminal (en mode texte) de
votre sous-sytème Linux...

![image remote wsl](data/wsl-status-bar.png)

Voici un petit résumé vidéo de tout cela : <https://www.youtube.com/watch?v=voJlmtoU7Lc>

Evidemment, si vous disposez d'un système Linux natif, tout cela ne sert à rien :-)

### Bureau à distance avec X2GO

Une autre solution consiste à travailler depuis son ordinateur personnel sur un
bureau graphique distant grâce à [X2GO au
CREMI](https://services.emi.u-bor.deaux.fr/intranet/spip.php?article125).
Attention, cette solution est instable et nécessite de disposer d'une excellente
connexion réseau (fibre à la maison).

**Exercice** : Reprenez le guide et lisez la section 10 du [Guide
SSH](https://gforgeron.gitlab.io/ssh/guide.pdf). Testez.

## Annexes

### Installation de WSL

Si vous disposez d'un ordinateur personnel sous Windows 10 (64 bits), il peut
être intéressant pour vous d'installer un sous-système Linux (WSL) de type
Debian ou Ubuntu.

Cette solution se base sur des techniques de virtualisation modernes ; elle est
très confortable car elle permet de travailler dans un vrai système Linux sans
avoir à mettre en place un *dual boot*. Néanmoins, son installation est un peu
plus compliquée et elle nécessite de posséder une machine suffisament puissante.

Pour installer WSL (version 2, de préférence), vous pouvez suivre ces tutoriels :

* <https://docs.microsoft.com/fr-fr/windows/wsl/install-win10>
* <https://korben.info/installer-wsl2-windows-linux.html>

Choisissez par exemple d'installer un sous-système Debian ou Ubuntu pour
disposer d'un configuration similaire au CREMI (cf. [Microsoft
Store](https://aka.ms/wslstore))

Puis installer dans votre Linux les paquets nécessaires, comme par exemple le
client `ssh`, le compilateur `gcc`, le débogeur `gdb`, `make`, etc :

```
$ sudo apt-get update
$ sudo apt-get install ssh gcc gdb build-essential
```

Depuis l'explorateur de fichier Windows, il est possible d'accéder aux fichiers
du sous-système Linux en tapant ce chemin `\\wsl$`, et réciproquement vous
pouvez accéder aux fichiers Windows depuis WSL directement à partir de `/mnt/c`.

**Attention** : Il est nécessaire pour faire fonctionner WSL d'activer la
virtualisation dans le BIOS, ainsi que Hyper-V dans Panneau Configuration /
Windows Feature. Vous pouvez vérifier qu'un *hyperviseur* est installé avec la
commande *systeminfo* (sous PowerShell en mode Administrateur).

## Ressources Complémentaires

* [Télétravail au CREMI](https://services.emi.u-bordeaux.fr/intranet/spip.php?article175)
* [Guide SSH](https://gforgeron.gitlab.io/ssh/guide.pdf) (UF Info Team)
* [Démo de connexion SSH au CREMI](https://www.youtube.com/watch?v=tjlhLdOqx38) (vidéo de A. Esnard)
* [Un autre tutoriel sur SSH](http://aurelien.esnard.emi.u-bordeaux.fr/ssh/tuto-ssh.pdf) (A. Esnard)
* [Travailler à distance, c'est possible !](https://reussir.u-bordeaux.fr/course/view.php?id=12) (Université de Bordeaux)
* [X2GO au CREMI](https://services.emi.u-bordeaux.fr/intranet/spip.php?article125)
* [Utilisation de VirtualBox au CREMI](virtualbox.html)
* [Application graphique avec WSL](wsl-gui.html)
* [Utilisation de VS Code avec l'extension Remote WSL](https://www.youtube.com/watch?v=voJlmtoU7Lc) (vidéo de A. Esnard)

---
