# Stage Environnement de Travail

Inscrivez-vous sur Moodle : <https://moodle1.u-bordeaux.fr/course/view.php?id=5858> (clé : stgenv).

Les ressources sont également disponibles sur cette page : <https://gitlab.inria.fr/stage-env/support>

Page Web : <https://stage-env.gitlabpages.inria.fr/support/>

**Niveau 1**

* Titre : Outils de Communication Académiques
* Public : ouvert à tous, obligatoire pour les nouveaux arrivants de L2 / L3
* Mots-clés : ENT, Moodle, Mail, Wifi Eduroam, Chat, Visio, Services CREMI, ...
* Sujet : [Niveau 1](niveau1.md)

**Niveau 2**

* Titre : "Environnement de Travail du CREMI"
* Public : ouvert à tous
* Mots-clés : unix, shell, système de fichiers, éditeur de texte, ...
* Sujet : [Niveau 2](niveau2.md)

**Niveau 3**

* Titre : "Travail à Distance"
* Public : ouvert à tous
* Mots-Clés : SSH, environnement de développement, WSL, ...
* Sujet : [Niveau 3](niveau3.md)

**Niveau 4**

* Titre : "Environnement de Travail Avancé"
* Public : en priorité les L3 Info
* Mots-Clés : processus, script shell, ...
* Sujet : [Niveau 4](niveau4.md)

**A Propos de ce Document**

Afin de favoriser l'accessibilité de ce cours, les supports de TD sont écrits en
Markdown (https://www.markdownguide.org/basic-syntax/). Ce document est
automatiquement convertit en site web avec l'outil
[MkDocs](https://www.mkdocs.org/).

---

**Coordinateurs**

* Aurélien Esnard (aurelien.esnard@u-bordeaux.fr)
* Pierre-André-Wacrenier (pierre-andre.wacrenier@u-bordeaux.fr)
