# Virtualisation

## Utilisation d'une machine virtuelle VirtualBox au CREMI

Nous allons dans un premier nous familiariser avec l'utilisation de *VirtualBox*
pour lancer une machine virtuelle contenant un environnement Linux semblable à
celui du CREMI. Cette machine contient un compte utilisateur dont le login est
*user* et le mot-de-passe est *user*. D'autre part, le mot-de-passe de
l'administrateur (login *root*) est *root*. Enfin l'utilisateur *user* a le
droit d'utiliser la commande *sudo*  pour lancer des commandes avec les droits
de l'administrateur.

Pour lancer la machine virtuelle il faut :

* Lancer le script dont le nom est *configuration_vbox_cremi.sh* et se trouvant
  dans */net/ens/VirtualBox/*. Ce script va configurer VirtualBox.
  * Lancer un terminal
  * Aller dans le dossier */net/ens/VirtualBox/* en tapant la commande *cd
    /net/ens/VirtualBox/*.
  * Lancer le script en tapant la commande *./configuration_vbox_cremi.sh*.
* Lancer l'application VirtualBox.

![Image VirtualBox 1](data/vbox1.png)

* Lancer la machine en cliquant sur le bouton *Démarrer*.

![Image VirtualBox 2](data/vbox2.png)

* Vous avez maintenant accès à une machine virtuelle linux dans laquelle vous
  pouvez lancer des applications. Vous avez de plus accès à internet.
* Il est possible d'échanger des fichiers entre la machine virtuelle et la
  machine physique en utilisant le dossier de partage : Tout fichier (ou
  dossier) copié dans ce dossier sera accessible à la fois par la machine
  virtuelle et par la machine physique. Le dossier d'échange peut être configuré
  en utilisant le menu de configuration de la machine virtuelle. On peut voir
  que le dossier /tmp de la machine physique est partagé avec la machine
  virtuelle. Il correspondra au dossier /media/sf_partage sur la machine
  virtuelle.

![Image VirtualBox 3](data/vbox3.png)

* Il est possible de configurer le chemin du dossier partagé sur la machine
  physique de la manière suivante (il suffit de remplacer /tmp par autre chose)
  :

![Image VirtualBox 4](data/vbox4.png)

![Image VirtualBox 5](data/vbox5.png)

* Il est possible de changer le nom du dossier du point de vue de la machine
  virtuelle en modifiant le champ Nom du dossier: Si vous donnez xxxx comme nom
  de dossier, le dossier sera accessible via le chemin /media/sf_xxxx sur la
  machine virtuelle.

## Utilisation d'une machine virtuelle VirtualBox sur mon ordinateur personnel

Nous allons configurer VirtualBox pour qu'il soit possible d'utiliser la machine
virtuelle que nous avons utilisée ci-dessus sur votre ordinateur personnel. Pour
ce faire, il faut :

* Installer VirtualBox sur votre ordinateur personnel
  * Sous Windows et MacOS, en suivant les instructions fournies sur le site de
    VirtualBox
  * Sous Linux, en installant le paquet VirtualBox à l'aide du gestionnaire de
    paquet de votre distribution
* Télécharger l'image contenant la machine virtuelle. Vous avez le choix entre deux méthodes de téléchargement:
  * En utilisant la commande scp  pour copier l'image qui se trouve au CREMI
    dans le dossier  /net/ens/VirtualBox/image-home/. Ceci peut se faire de la
    manière suivante (`<login>` est votre login au CREMI et `<chemin-destination>`
    l'endroit où vous voulez stocker l'image sur votre machine personnelle): ```scp
    <login>@jaguar.emi.u-bordeaux.fr:/net/ens/VirtualBox/image-home/debian.ova
    <chemin-destination>```
  * En téléchargeant l'image depuis l'URL suivante:
    https://filesender.renater.fr/?s=download&token=eb3d0ec4-1490-4331-917f-b72e10be3088
    (ce lien expire le 09/10/2020).
* Il faut maintenant lancer VirtualBox et importer le fichier que vous venez de télécharger...

![Image VirtualBox 6](data/vbox6.png)

![Image VirtualBox 7](data/vbox7.png)

![Image VirtualBox 8](data/vbox8.png)

![Image VirtualBox 9](data/vbox9.png)

![Image VirtualBox 10](data/vbox10.png)

![Image VirtualBox 11](data/vbox11.png)

* Une fois l'opération terminée. La machine Debian a été correctement ajoutée.
* Il est maintenant possible de la lancer sur votre machine et de configurer le
  dossier de partage comme expliqué dans la section précédente. Rermarque: Le
  dossier de partage doit être un dossier que vous avez créé spécifiquement à
  cet effet (vous pouvez le nommer partage) afin de minimiser les erreurs de
  manipulation.

---
