# Niveau 1

## IDNum (Identité Numérique)

Si vous êtes un nouvel arrivant et que vous avez bien finalisé votre
inscription administrative, il vous faut activer votre IDNum :

<https://idnum.u-bordeaux.fr/etudiant>

Vous aurez besoin de votre code étudiant et de votre INE, qui figurent sur le certificat de scolarité.

Connaître son identité numérique :

* Quel est votre identifiant de connexion (ou *login*) ?
* Quel est votre mot de passe personnel ? Chut, il ne faut surtout pas le dire !
* Quel est votre adresse mail académique ? Elle doit être de la forme : prenom.nom@etu.u-bordeaux.fr

## ENT (Espace Numérique de Travail)

Connectez-vous à l'ENT : <https://ent.u-bordeaux.fr>

Parcourez les menus et retrouvez l'accès à la plateforme pédagogique Moodle,
l'emploi du temps, l'annuaire, le webmail, le canal de stockage, ...

* Dans l'annuaire, retrouvez l'adresse mail de l'enseignant et celui de votre
  voisin...
* Dans l'emploi du temps [Celcat](https://celcat.u-bordeaux.fr/), retrouvez vos
  cours de la semaine prochaine, par groupe (ex. IN301A ou IN301A11 pour la L2
  Info), par module (indiquez le code UE, ex. 4TIN303U pour la Prog C en L2
  Info).
* Pour consulter l'emploi du temps de son groupe, il existe également une
  application smartphone très pratique [Ukit
  Bordeaux](https://ukit-bordeaux.fr/).
* Créez un fichier sur votre *répertoire de connexion* qu'on appelle
  traditionnellement *home*. Puis, retrouvez ce fichier en utilisant le canal de
  stockage.
* Grâce au Webmail (accessible directement à https://webmel.u-bordeaux.fr),
  envoyez un courriel à votre voisin avec un document en pièce-jointe. À quoi
  correspondent les champs Cc et Cci (ou Bcc en anglais) ?
* Pour aller plus loin : apprenez à intéger votre agenda universitaire (flux
  ICS) dans une autre application comme Google Agenda, Thunderbird ou votre
  smartphone...

## Client de Messagerie

Le Webmail est très pratique, mais il est souvent plus confortable et plus
puissant d'utiliser sur son PC (ou même smartphone) une application dédiée, ou
*client de messagerie* dit *lourd*, comme par exemple *Evolution* ou *Thunderbird*.

Au CREMI, lancez *Thunderbird* et configurez le client de messagerie pour qu'il
accède à vos courriels. Voici les principaux paramètres de configuration que
vous devez saisir :

* **Adresse** : prenom.nom@etu.u-bordeaux.fr (exemple:
  anne.onyme@etu.u-bordeaux.fr)
* **Protocole** : IMAP (Attention à ne pas utiliser POP3 !)
* **Serveur entrant (IMAP)** : webmel.u-bordeaux.fr, mode sécurisé SSL/TLS, port
  993, authentification par mot de passe normal
* **Serveur sortant (SMTP)** : smtpauth.u-bordeaux.fr, mode sécurisé SSL/TLS,
  port 465, authentification par mot de passe normal
* **Identifiant** : il faut remplacer "prenom.nom" par votre identifiant ENT :
  le plus souvent, c'est l'initiale de votre prénom accolée à votre nom (exemple
  : aonyme)
* **Mot de passe** : il faudra ultérieurement saisir votre mot de passe ENT.

![Image Client de Messagerie](data/config-mail.png)

**Documentation** : <https://webmel.u-bordeaux.fr/Wiki/ub.php?id=parametrage:clients_lourds>

## Gestionnaire de Fichiers

A l'aide du *gestionnaire de fichiers* (application graphique), vérifiez que
vous arrivez à réaliser les opérations suivantes :

* crééer des répertoires (Ctrl+Shift+N),
* créér des fichiers textes,
* renommer un fichier ou un répertoire (F2),
* copier (Ctrl-c), couper (Ctrl-x), coller (Ctrl-v), supprimer (Suppr),
* compresser au format Zip (menu contextuel, clic droit),
* ouvrir le Terminal dans le répertoire courant (clic droit).

## Wi-Fi Eduroam

Késako ? <https://www.eduroam.fr/>

Vous pouvez utiliser ce réseau Wi-Fi gratuitement un peu partout en France et
dans le monde ! (Vous le trouverez surtout dans les universités et certains
établissements publics.)

Si vous êtes venu avec votre ordinateur portable, ou simplement si vous avez un
*smartphone*... Activez le Wi-Fi et connectez-vous au réseau Eduroam, en
configurant les paramètres suivants :

* **Sécurité** : WPA & WPA2 Enterprise
* **Authentification Phase 1** : Tunneled TLS (TTLS)
* **Certificat CA**: aucun, il convient parfois d'indiquer "No CA is required".
* **Identité Anonyme** : anonymous@u-bordeaux.fr
* **Authentification Phase 2** : PAP
* **Identifiants** : il faut utiliser son *login* suivi de *@u-bordeaux.fr*, par
  exemple *auesnard@u-bordeaux.fr* si mon  login est auesnard (il ne s'agit pas
  pas son adresse mail)
* **Mot de Passe** : celui du votre compte de l'Université de Bordeaux (IDNum)

![Image Config Eduroam](data/config-eduroam.png)

Sous Mac, le plus simple est d'utiliser l'installeur d'eduroam :  <https://cat.eduroam.org/>

Avec un smartphone (sous IOS, notamment), le plus simple est de consulter
directement la page <https://www.eduroam.fr/> pour configurer le Wifi.

Un peu d'aide supplémentaire ? <https://www.u-bordeaux.fr/Campus/Services-numeriques/Wifi2/EDUROAM>

## Moodle

Moodle, c'est là plateforme pédagogique de l'Université de Bordeaux, très
largement utilisé en Licence Informatique.

Plusieurs activités seront très utilisées tout au long de l'année...

* Choix de Groupe,
* Questionnaire (ex. QCM),
* Sondage, Sondage Anonyme (ex. évaluation des enseignements),
* Rendu de Devoir (ex. rapports de projet),
* VPL (ex. correction automatisée de programmes).

L'activité VPL permet aux étudiants d'éditer du code en ligne (ou de l'importer
à partir d'un fichier) afin d'évaluer ce code automatiquement sur la base d'un
script produit par vos enseignants. Trois boutons *run*, *debug*, *eval* vous
permettent de tester votre programme. En fonction de la configuration de
l'activité, ces boutons ne sont pas toujours disponibles. À l'issue de la phase
d'évaluation, une note vous est atttribuée, que vous pouvez retrouver dans le
*Carnet de Notes*.

![Image VPL](data/moodle-vpl-edit.png)

Testez dans la page Moodle les activités proposées :

* *Rendre un Devoir* ;
* *Rendre un Programme*.

**Astuces** : Pour bien utiliser Moodle, prenez l'habitude de consulter vos
résultats dans le *Carnet de Notes*, ainsi que les futures *deadline* dans le
*Calendrier* !

## Outils de Communication

### Discussion en Ligne (Chat)

L'Université de Bordeaux met à votre disposition un serveur **RocketChat**
(logiciel libre), que vous pouvez utiliser simplement avec votre navigateur :
<https://chat.u-bordeaux.fr>

Comme pour la plupart des outils de ce type, les discussions sont organisées en
canaux (ou *channel*) et il faut généralement recevoir une invitation pour
participer à un channel privé... Testez par exemple cette invitation à un
channel de test :

<https://go.rocket.chat/invite?host=chat.u-bordeaux.fr&path=invite%2FDBioyf>

Notez par ailleurs qu'il existe un client *Desktop* pour Linux, Mac et Windows,
ainsi qu'un application Mobile Android & IOS. Dans tous les cas, il vous faudra
renseigner l'adresse du serveur <chat.u-bordeaux.fr>.

Il existe de nombreux outils alternatifs, comme par exemple Mattermost ou
Discord.

### Cours en Visio

Les cours en visio vont s'appuyer sur l'outil Zoom (non libre), pour lequel
l'Université de Bordeaux a souscrit une licence : <https://u-bordeaux-fr.zoom.us/>

Demandez à votre enseigant de démarrer une session Zoom, et rejoignez cette
session... Pour effectuer ce test, l'idéal est d'utiliser des écouteurs avec un
micro intégré (écouteur typique de smartphone).

*Nota Bene* : L'activité Zoom dans Moodle permet de rejoindre facilement un
*meeting* Zoom préparé par vos enseignants, comme celui déjà présent sur la page
Moode...

Un outil alternatif (libre) également disponible à l'Université de Bordeaux est
BigBlueButton (BBB) : <https://webconf.u-bordeaux.fr/b/>. A priori, on
n'utilisera pas cet outil.

Plus d'info : <https://personnels.u-bordeaux.fr/Metiers/Services-numeriques/Zoom-ZoomBox-ZoomRoom>

## Les Services du CREMI

* Les services Intranet : <https://services.emi.u-bordeaux.fr/allservices/>
  (authentification requise pour accéder à l'Intranet)
* Consultez les informations relative à votre compte informatique au CREMI :
  <https://services.emi.u-bordeaux.fr/intranet/mesinfos/>
* Notez votre quota de stockage...
* Notez l'adresse de votre *site web* académique, qui doit être de la forme :
  <https://prenom-nom.emi.u-bordeaux.fr>. À quel répertoire est-il associé sur
  votre compte Linux ? Ajoutez un fichier *index.html* qui contient un message
  personnel, comme par exemple "Hello World!"... Attention, ce message est
  public ! Consultez ensuite votre page web avec un navigateur web !
* Les tickets ? En cas de problème (machine en panne, matériel cassé, problème
  divers, ...), vous pouvez demander de l'assistance auprès des administrateurs
  du CREMI en remplissant le formulaire suivant : <https://services.emi.u-bordeaux.fr/glpi/>
* Imprimer au CREMI :
  <https://services.emi.u-bordeaux.fr/intranet/spip.php?article174>

## Pour aller plus loin

Démarrer le Niveau 2...
