# TODO

## Todo List

* Ajouter / compléter les tests de positionnement....
* Ajouter l'extension *OCaml Platform / Formatter* pour VSCode, afin de
  n'utiliser qu'un seul IDE et de se passer de Emacs, jugé trop compliqué par
  les étudiants, ... REPL avec `Shift Enter`, ... Ajouter une activité OCaml de
  test...
* De nouveaux prérequis exprimés, il faudrait ajouter quoi ? un peu de *Git*
  avec le Gitlab du CREMI, de *Latex* avec Overleaf, ...
### Réunion du 18 juin 2021

* ...
* ...
* ...

## Bilan 2020-2021

Le stage s'est déroulé la semaine de la rentrée (Sem. 37).

Le public visé est d'environ 500 étudiants, inscrits sur Moodle. Il s'agit des
L2/L3 des parcours Info, Math-Info, OPTIM, ISI. Environ 400 étudiants ont
visités la page Moodle... Certains étudiants ont effectués les TDs à distance,
sans venir au CREMI.

Stage découpé en 4 niveaux avec inscription libre des étudiants sur Moodle :

* 1 CM de 1h20 répété 4 fois...
* Niveau 1 : 5 créneaux de 1h20 (TDM) => 42 étudiants inscrits
* NIveau 2 : 12 créneaux de 1h20 (TDM) => 164 étudiants inscrits
* Niveau 3 : 12 créneaux de 1h20 (TDM) => 179 étudiants inscrits
* Niveau 4 : 5 créneaux de 1h20 (TDM) => 67 étudiants inscrits

En pratique, le nombre d'étudiants présent est souvent supérieurs aux inscrits
sur Moodle. Certains créneaux ont été fusionné pour regrouper les étudiants dans
une même salle avec plusieurs enseignants, ce qui a été très confortable surtout
pour le *niveau 1*.

**Auto-Evaluation**

Un questionnaire Moodle (anonyme) a été transmis auprès des étudiants pour
évaluer le taux de satisfcation et chercher des pistes d'amélioration pour la
suite.

**Répartition TDM**

Un système d'inscription libre des enseignants sur Moodle a également été mis en
place. Chaque enseignant choisissait un ou plusieurs créneaux libre en fonction
de son agenda personnel.

* Bonichon, Nicolas : 2 N1 + 2 N2 + 1 N3 = 5 TDM (1h20) = 6h40 heqtd
* Durand, Irene : 1 N1 + 4 N2 =  5 TDM (1h20) = 6h40 heqtd
* Esnard, Aurelien : 1 N1 + 2 N2 + 3 N3 + 1 N4 = 7 TDM (1h20) = 9h20 heqtd
* Wacrenier, Pierre-Andre : 1 N1 + 2 N2 + 2 N4 = 5 TDM (1h20) = 6h40 heqtd
* Thibault, Samuel : 2 N2 + 2 N3 + 1 N4  = 5 TDM (1h20) = 6h40 heqtd
* Auber, David : 2 N3 = 2 TDM (1h20) = 2h40 heqtd
* Duchon, Philippe : 2 N3 =  2 TDM (1h20) = 2h40 heqtd
* Reveillere, Laurent : 2 N3 + 1 N4 =  3 TDM (1h20) = 4h00 heqtd

Coût = 45h20 heqtd

**Répartition CM**

Le CM s'est déroulé à la suite de la réunion de rentrée des L2 et L3, répétés
deux fois dans le *Grand Amphi de Math* pour respecter les contraintes
sanitaires.

* Esnard, Aurelien : 2 CM (1h20) = 4h00 heqtd
* Wacrenier, Pierre-Andre : 1 CM (1h20 )= 2h00 heqtd
* Namyst, Raymond : 1 CM (1h20) = 2h00 heqtd

Coût = 8h00 heqtd

**Divers Charges**

* Esnard, Aurelien : Coordination et Production de Ressources = 6h00 heqtd
* Wacrenier, Pierre-Andre : Coordination et Production de Ressources = 2h40 heqtd
* Guermouche, Abdou : Production de Ressources (virtualbox) = 1h20 heqtd

Coût = 10h00 heqtd

**Coût Total** :  63h20 heqtd

## Questionnaire Evaluation

* Quel est votre parcours ? L2 Info / ...
* Avez-vous participé au stage ?
  * j'ai fait un ou plusieurs TDs en présentiel (et éventuellement le reste à distance).
  * j'ai fait un ou plusieurs TDs à distance, en autonomie.
  * je n'y ai pas participé... Précisez pourquoi ?
* Est-ce que vous juger ce stage utile pour vous ?
* Taux de Satisfaction : Comment appréciez-vous le stage environnement dans sa globalité ? (Satisfaisant/.../NSP)
* Quel suggestion d'amélioration pouvez-vous faire pour la suite ?

