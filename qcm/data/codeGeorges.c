#include <stdio.h>
#define N 23
int main() {
    int somme = 0;
    for (int i = 1; i <= N; i++) {
        somme += 2*i * (i-1)/2;
    }
    printf("%d\n", somme);
    return 0;
}
