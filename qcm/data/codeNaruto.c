#include <stdio.h>
#define N 45
int main() {
    int somme = 0;
    for (int i = 1; i <= N; i++) {
        somme += 3*i * (i-1);
    }
    printf("%d\n", somme);
    return 0;
}
